 
Option Explicit
Dim objProcess, objWMIService, colProcesses, Process, ProcessorTime, DynaMailOn 
Dim strBody, iMsg, iConf, Flds, DataList, strComputer



DynaMailOn = 0
strComputer = "."

Set objWMIService = GetObject("winmgmts:" _
 & "{impersonationLevel=impersonate}!\\" & strComputer& "\root\cimv2")
Set colProcesses = objWMIService.ExecQuery _
 ("SELECT * FROM Win32_Process")
For Each Process in colProcesses
	If Process.Name="" then 
		DynaMailOn=DynaMailOn+1
	End If
next

If DynaMailOn = 0 then call MailSub


Sub MailSub()
  Set iMsg = CreateObject("CDO.Message")
  Set iConf = CreateObject("CDO.Configuration")
  Set Flds = iConf.Fields

  Const schema = "http://schemas.microsoft.com/cdo/configuration/"

  Flds.Item(schema & "sendusing") = 2
  Flds.Item(schema & "smtpserver") = "smtp.gmail.com" 
  Flds.Item(schema & "smtpserverport") = 465
  Flds.Item(schema & "smtpauthenticate") = 1
  Flds.Item(schema & "sendusername") = ""
  Flds.Item(schema & "sendpassword") = ""
  Flds.Item(schema & "smtpusessl") = 1
  Flds.Update

  With iMsg
    .To = ""
    .From = ""
    .Subject = "DynaMailGenerator.exe has failed at:" & Now
    .HTMLBody = "The DynaMailGenerator process has stopped running.  Please restart the process." 
    .Sender = "Script Monitoring"
    .Organization = ""
    .ReplyTo = ""
 
    Set .Configuration = iConf

    .Send 
  End With 
 
 ' release interfaces
  Set iMsg = nothing
  Set iConf = nothing
  Set Flds = nothing
  Set DynaMailOn = nothing
End Sub
