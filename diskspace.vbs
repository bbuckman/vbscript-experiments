OPTION EXPLICIT
On error resume next

CONST strReport = "c:\diskspace.txt"


DIM objWMIService, objItem, colItems, arrComputers, strComputer
DIM strDriveType, strDiskSize, txt, txt2
arrComputers = Array("app001","www001","www002","www003","www004","app002","app003","sql001a","sql002b","stg001")
 
For Each strComputer In arrComputers
wscript.echo strComputer
SET objWMIService = GETOBJECT("winmgmts:\\" & strComputer & "\root\cimv2")
SET colItems = objWMIService.ExecQuery("Select * from Win32_LogicalDisk WHERE DriveType=3")
txt = "Drive" & vbtab & "Size" & vbtab & "Used" & vbtab & "Free" & vbtab & "Free(%)" & vbcrlf
FOR EACH objItem in colItems
	
	DIM pctFreeSpace,strFreeSpace,strusedSpace
	
	pctFreeSpace = INT((objItem.FreeSpace / objItem.Size) * 1000)/10
	strDiskSize = Int(objItem.Size /1073741824) & "Gb"
	strFreeSpace = Int(objItem.FreeSpace /1073741824) & "Gb"
	strUsedSpace = Int((objItem.Size-objItem.FreeSpace)/1073741824) & "Gb"
	txt = txt & objItem.Name & vbtab & strDiskSize & vbtab & strUsedSpace & vbTab & strFreeSpace & vbtab & pctFreeSpace & vbcrlf
        txt2 = txt2 & vbcrlf & strComputer & vbcrlf & txt
NEXT

writeTextFile txt2, strReport
wscript.echo "Report written to " & strReport & vbcrlf & vbcrlf & txt
NEXT

PRIVATE SUB writeTextFile(BYVAL txt,BYVAL strTextFilePath)
	DIM objFSO,objTextFile
	
	SET objFSO = CREATEOBJECT("Scripting.FileSystemObject")

	SET objTextFile = objFSO.CreateTextFile(strTextFilePath)

	objTextFile.Write(txt2)

	objTextFile.Close
	SET objTextFile = NOTHING
END SUB