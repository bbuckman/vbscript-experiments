'----------------------------------------------------------
'JoinDomain.vbs
'Script to join a workgroup computer onto a Domain
'----------------------------------------------------------
 
'Variable definition
Const JOIN_DOMAIN             = 1
Const ACCT_CREATE             = 2
Const ACCT_DELETE             = 4
Const WIN9X_UPGRADE           = 16
Const DOMAIN_JOIN_IF_JOINED   = 32
Const JOIN_UNSECURE           = 64
Const MACHINE_PASSWORD_PASSED = 128
Const DEFERRED_SPN_SET        = 256
Const INSTALL_INVOCATION      = 262144
 
' Enter domain name
strDomain   = "domain.local"
' Enter Username
strUser     = "administrator"
' Enter a user password
strPassword = ""
 
Set objNetwork = CreateObject("WScript.Network")
 
strComputer = objNetwork.ComputerName
 
Set objComputer = _
    GetObject("winmgmts:{impersonationLevel=Impersonate}!\\" & _
    strComputer & "\root\cimv2:Win32_ComputerSystem.Name='" _
    & strComputer & "'")
 
ReturnValue = objComputer.JoinDomainOrWorkGroup(strDomain, _
    strPassword, _
    strDomain & "\" & strUser, _
    NULL, _
    JOIN_DOMAIN + ACCT_CREATE)

MsgBox "Joined Domain.Local Domain!"
 
Set objWMIService = GetObject("winmgmts:" _
& "{impersonationLevel=impersonate,(Shutdown)}!\\" & strComputer & "\root\cimv2")
Set colOperatingSystems = objWMIService.ExecQuery _
("Select * from Win32_OperatingSystem")
For Each objOperatingSystem in colOperatingSystems
ObjOperatingSystem.Reboot()
Next
