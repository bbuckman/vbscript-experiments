Dim fileSize, fileSize2, totalSize, checkFile, strComputer, objWMIService
Dim setFormat, MessageTitle, messageBody, SizeFormat

Set WshShell = CreateObject("WScript.Shell")
Set objNetwork = CreateObject("Wscript.Network")
Set fso = CreateObject("Scripting.FileSystemObject")

strComputer = "."
Set objWMIService = GetObject("winmgmts:" _
& "{impersonationLevel=impersonate}!\\" & _
strComputer & "\root\cimv2")

Set colMonitoredEvents = objWMIService.ExecNotificationQuery _
("SELECT * FROM __InstanceModificationEvent WITHIN 10 WHERE " _
& "TargetInstance ISA 'CIM_DataFile' and " _
& "TargetInstance.Name='e:\\ExchangeDB\\MDBDATA\\priv1.edb'")

Do
Set objLatestEvent = colMonitoredEvents.NextEvent
filesize = 0
filesize2 = 0
totalSize = 0

target = "e:\ExchangeDB\MDBDATA\priv1.stm"
Set checkFile = fso.GetFile(target)

fileSize = SetBytes(checkFile.size)
filesize2 = SetBytes(objLatestEvent.TargetInstance.FileSize)

totalSize = filesize + filesize2
Wscript.Echo "Exchange DB size is" & totalSize & SizeFormat

If (filesize + filesize2 ) > 15 Then
messageBody = "Danger Will Robinson! Exchange DB size at " & totalSize & SizeFormat & vbCrlf & vbCrlf _
& "Please restart this script on the server."
SendEmail()
WScript.Quit()
End If
Loop

'-------------------------
Function SetBytes(Bytes)
If Bytes >= 1073741824 Then
SetBytes = Round(FormatNumber(Bytes / 1024 / 1024 / 1024, 2), 2)
SizeFortmat = "GB"
ElseIf Bytes >= 1048576 Then
SetBytes = Round(FormatNumber(Bytes / 1024 / 1024, 2), 2)
SizeFortmat = "MB"
ElseIf Bytes >= 1024 Then
SetBytes = Round(FormatNumber(Bytes / 1024, 2), 2)
SizeFortmat = "KB"
ElseIf Bytes < 1024 Then
SetBytes = Bytes
SizeFortmat = "Bytes"
Else
SetBytes = "0 Bytes"
End If
End Function