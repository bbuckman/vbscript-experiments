Const ADS_SCOPE_SUBTREE = 2

Set objConnection = CreateObject("ADODB.Connection")
Set objCommand = CreateObject("ADODB.Command")

objConnection.Provider = "ADsDSOOBject"
objConnection.Open "Active Directory Provider"

Set objCommand.ActiveConnection = objConnection
Set objRootDSE = GetObject("LDAP://RootDSE")

strDNSDomain = objRootDSE.Get("DefaultNamingContext")
strBase = "<LDAP://" & strDNSDomain & ">"
strFilter = "(&(ObjectCategory=Computer))"
strAttributes = "name, distinguishedName"
strQuery = strBase & ";" & strFilter & ";" & strAttributes & ";SubTree"

objCommand.CommandText = strQuery
objCommand.Properties("Page Size") = 99999
objCommand.Properties("Timeout") = 300
objCommand.Properties("Cache Results") = False

Set objRecordSet = objCommand.Execute
objRecordSet.MoveFirst
Do Until objRecordSet.EOF
Set objComputer = GetObject("LDAP://" & objRecordSet.Fields("distinguishedName") & "")
dtmValue = objComputer.Name
MsgBox dtmValue
objRecordSet.MoveNext
Loop