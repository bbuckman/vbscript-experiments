' Region Description
'
' Name: List Mailboxes vs AD Accounts
' 
' Version: 1.0
' Description: Used to show Mailboxes on the Exchange DB that dont have an AD Account
' 
' Standard Variable Description
' g_ = Global Varibales
' str = String
' int = Integer
' obj = Object
' bln = Boolean
'
' EndRegion

Option Explicit

' Const
Const adBoolean = 11
Const adInteger = 3
Const ForAppending = 8
Const ForWriting = 2
Const adVarChar = 200
Const adCmdStoredProc = 4
Const adLockReadOnly = 1
Const adOpenDynamic = 2
Const excelAlignDistributed = 1
Const excelAlignLeft = 2
Const excelAlignCenter = 3
Const excelAlignRight = 4
Const excelAlignJustify = 5
' EndConst

Dim g_objADAccounts, g_objExchangeMailboxes

Main


Sub Main
	
	CreateADOObjects
	
	GetExchangeMailboxes
	
	GetADAccounts
	
	CompareExchangevsAD
	
	ReportMissingADAccounts

End Sub


Sub CreateADOObjects
	
	Set g_objADAccounts = CreateObject("ADODB.RecordSet") 
	g_objADAccounts.Fields.Append "SAMAccountName", adVarChar, 255
	g_objADAccounts.Open
	
	Set g_objExchangeMailboxes = CreateObject("ADODB.RecordSet") 
	g_objExchangeMailboxes.Fields.Append "SAMAccountName", adVarChar, 255
	g_objExchangeMailboxes.Fields.Append "ADAccountFound", adBoolean
	g_objExchangeMailboxes.Open

End Sub

Sub GetExchangeMailboxes

	Dim strComputer, objWMIService, colItems, objItem

	strComputer = "."
	Set objWMIService = GetObject("winmgmts:" _
		& "{impersonationLevel=impersonate}!\\" & strComputer & _
			"\ROOT\MicrosoftExchangeV2")
	
	Set colItems = objWMIService.ExecQuery _
		("Select * from Exchange_Mailbox")
	
	For Each objItem In colItems
		
		'WScript.Echo Mid(objItem.LegacyDN, InStrRev(objItem.LegacyDN, "=") + 1)
		g_objExchangeMailboxes.AddNew
		'g_objExchangeMailboxes.Fields("SAMAccountName") = Mid(objItem.LegacyDN, InStrRev(objItem.LegacyDN, "=") + 1)
		g_objExchangeMailboxes.Fields("SAMAccountName") = objItem.MailboxDisplayName
		g_objExchangeMailboxes.MoveNext
	
	Next
					
	WScript.Echo "Mailbox count: " & g_objExchangeMailboxes.RecordCount
End Sub

Sub GetADAccounts
	
	Dim strSearchObjectType, strSearchAttribute, strReturnAttribute, strSearchValue
	Dim objRootDSE, objConnection, objCommand, objADAccounts
	Dim strRecordCount 
	Dim intADAccounts
	
	'Sub Variables
	intADAccounts = 0
			
	strSearchObjectType = "Person"																				'Set to Person or Computer 
	strSearchAttribute = "distinguishedname"			 					 									'Attribute you want to use to search
	strSearchValue = "*"																						'Value used to search
	strReturnAttribute = "SAMAccountName,distinguishedname,employeeID,displayname,AccountExpires,Description"	'Attributes you want returned
		
	Set objRootDSE = GetObject("LDAP://rootDSE") 
	Set objConnection = CreateObject("ADODB.Connection") 
	objConnection.Open "Provider=ADsDSOObject;" 
	
	Set objCommand = CreateObject("ADODB.Command") 
	Set objCommand.ActiveConnection = objConnection
	objCommand.Properties("Page Size") = 1000
	
	objCommand.CommandText = "<LDAP://" & objRootDSE.get("defaultNamingContext") & _ 
		">;(&(objectCategory=" & strSearchObjectType & ")(objectClass=user)(" & strSearchAttribute & "=" & strSearchValue & "));" & strReturnAttribute & ";subtree" 
	
	Set objADAccounts = objCommand.Execute 
	
	'Move to First Record
	objADAccounts.MoveFirst
	
	'Loop through the returned record set
	Do While Not objADAccounts.EOF
		'WScript.Echo objADAccounts.Fields("SAMAccountName")
		If Not IsNull(objADAccounts.Fields("displayname")) Then
			g_objADAccounts.AddNew
			g_objADAccounts.Fields("SAMAccountName") = objADAccounts.Fields("displayname")
		End If
		objADAccounts.MoveNext	
	Loop					
	
	WScript.Echo "AD Account Count: " & g_objADAccounts.RecordCount
	
	'Close Objects
	Set objCommand = Nothing
	Set objConnection = Nothing
	Set objRootDSE = Nothing
	Set objADAccounts = Nothing

End Sub

Sub CompareExchangevsAD
	
	Dim intFoundMailbox
	
	g_objExchangeMailboxes.MoveFirst
	Do While Not g_objExchangeMailboxes.EOF
		
		g_objADAccounts.MoveFirst
		Do While Not g_objADAccounts.EOF
			
			
			If UCase(g_objExchangeMailboxes.Fields("SAMAccountName")) = UCase(g_objADAccounts.Fields("SAMAccountName")) Then
				'WScript.Echo "Mailbox found For " & g_objADAccounts.Fields("SAMAccountName")
				g_objExchangeMailboxes.Fields("ADAccountFound") = True
			End If
		
			g_objADAccounts.MoveNext
		Loop

		g_objExchangeMailboxes.MoveNext
	Loop

End Sub

Sub ReportMissingADAccounts
	
	g_objExchangeMailboxes.MoveFirst
	
	Do While Not g_objExchangeMailboxes.EOF
		If g_objExchangeMailboxes.Fields("ADAccountFound") <> True Then
			WScript.Echo g_objExchangeMailboxes.Fields("SAMAccountName") & " was not found"
		End If
		g_objExchangeMailboxes.MoveNext
	Loop

End Sub

