

strComputer = "."
Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & _
        "\ROOT\MicrosoftExchangeV2")

Set colItems = objWMIService.ExecQuery _
    ("Select * from Exchange_Mailbox")

MailCount = colItems.count

dtmThisDay = Day(Date)
dtmThisMonth = Month(Date)
dtmThisYear = Year(Date)
dtmThisHour = hour(time)

if dtmthismonth < 10 then
    dtmthismonth = "0" & dtmthismonth
end if

if dtmthisday < 10 then
    dtmthisday = "0" & dtmthisday
end if

dtmbigdate = dtmThisyear & dtmThisMonth & dtmThisday & "000000.000000-300"

dtmTomDay = Day(Date + 1)
dtmTomMonth = Month(Date + 1)
dtmTomYear = Year(Date + 1)
dtmTomHour = hour(time + 1)

if dtmTommonth < 10 then
    dtmTommonth = "0" & dtmTommonth
end if

if dtmTomday < 10 then
    dtmTomday = "0" & dtmTomday
end if

dtmtomdate = dtmTomyear & dtmTomMonth & dtmTomday & "000000.000000-300"

strBackupName = dtmThisYear & "-" & dtmThisMonth & "-" & dtmThisDay
txtbdy = "Exhange Data Store:" & "<br>"



strComputer = "."
Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")

Set colFiles = objWMIService.ExecQuery _
    ("Select * from CIM_Datafile Where Path = '\\Program Files\\Exchsrvr\\Mdbdata\\' " & _
"AND Drive = 'd:'")



For Each objFile in colFiles

If objFile.Extension = "edb" or objfile.Extension = "stm" Then
        
    gbsize = CInt(objfile.filesize/1073741824)
    txtbdy = txtbdy & "File name: " & objFile.FileName & "." & objFile.Extension & "<br>"
    txtbdy = txtbdy & "File size: " & gbsize & "GB" & "<br>" & "<br>"
end if
 
Next

Call MailSub (MailCount)

Sub MailSub(MailCount)
  Set iMsg = CreateObject("CDO.Message")
  Set iConf = CreateObject("CDO.Configuration")
  Set Flds = iConf.Fields

  Const schema = "http://schemas.microsoft.com/cdo/configuration/"

  Flds.Item(schema & "sendusing") = 2
  Flds.Item(schema & "smtpserver") = "smtp.gmail.com" 
  Flds.Item(schema & "smtpserverport") = 465
  Flds.Item(schema & "smtpauthenticate") = 1
  Flds.Item(schema & "sendusername") = ""
  Flds.Item(schema & "sendpassword") = ""
  Flds.Item(schema & "smtpusessl") = 1
  Flds.Update

  With iMsg
    
    .To = ""
    .From = ""
    .Subject = "Mailbox Count as of " & Now
    .HTMLBody = "The number of Exchange mailboxes presently being used is "  & MailCount & ".  If this number exceeds 390, please take action.<br><br>"  & txtbdy
    .Sender = ""
    .Organization = ""
    .ReplyTo = ""
 
    Set .Configuration = iConf

    .Send 
  End With 
 
 ' release interfaces
  Set iMsg = nothing
  Set iConf = nothing
  Set Flds = nothing
  Set DynaMailOn = nothing
End Sub